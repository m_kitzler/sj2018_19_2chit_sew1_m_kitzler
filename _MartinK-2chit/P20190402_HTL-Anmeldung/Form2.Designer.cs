﻿namespace P20190402_HTL_Anmeldung
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listViewLogins = new System.Windows.Forms.ListView();
            this.columnHeaderFN = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderLN = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderEW = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderZW = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderTNumber = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SuspendLayout();
            // 
            // listViewLogins
            // 
            this.listViewLogins.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderFN,
            this.columnHeaderLN,
            this.columnHeaderEW,
            this.columnHeaderZW,
            this.columnHeaderTNumber});
            this.listViewLogins.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewLogins.Location = new System.Drawing.Point(13, 13);
            this.listViewLogins.MultiSelect = false;
            this.listViewLogins.Name = "listViewLogins";
            this.listViewLogins.Size = new System.Drawing.Size(508, 319);
            this.listViewLogins.TabIndex = 0;
            this.listViewLogins.UseCompatibleStateImageBehavior = false;
            this.listViewLogins.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderFN
            // 
            this.columnHeaderFN.Text = "Vorname";
            this.columnHeaderFN.Width = 100;
            // 
            // columnHeaderLN
            // 
            this.columnHeaderLN.Text = "Nachname";
            this.columnHeaderLN.Width = 100;
            // 
            // columnHeaderEW
            // 
            this.columnHeaderEW.Text = "Erstwahl";
            this.columnHeaderEW.Width = 100;
            // 
            // columnHeaderZW
            // 
            this.columnHeaderZW.Text = "Zweitwahl";
            this.columnHeaderZW.Width = 100;
            // 
            // columnHeaderTNumber
            // 
            this.columnHeaderTNumber.Text = "Telefonnummer";
            this.columnHeaderTNumber.Width = 100;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 344);
            this.Controls.Add(this.listViewLogins);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listViewLogins;
        private System.Windows.Forms.ColumnHeader columnHeaderFN;
        private System.Windows.Forms.ColumnHeader columnHeaderLN;
        private System.Windows.Forms.ColumnHeader columnHeaderEW;
        private System.Windows.Forms.ColumnHeader columnHeaderZW;
        private System.Windows.Forms.ColumnHeader columnHeaderTNumber;
    }
}