﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using at.htlkrems.anmeldung;

namespace P20190402_HTL_Anmeldung
{
    public partial class Form1 : Form
    {
        string EW = "";
        string ZW = "";
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSubmit_Click(object sender, EventArgs e)
        {
            if (textBoxFN.Text == "" || textBoxLN.Text == "" || EW == "" || ZW == "")
                MessageBox.Show("Ein oder mehrere Felder sind nicht ausgefüllt!", "He Trottl!");
            else if (EW == ZW)
                MessageBox.Show("Du kannst bei Erst- und Zweitwahl nicht das gleiche nehmen!", "Spinnst?");
            else if (MessageBox.Show(((EW[0] == 'I' && ZW[0] == 'I')) ? "Haben sie ihre Eingaben auf ihre Richtigkeit überprüft?" : "Du hast in mindestens einer deiner Wahlen Hoch- oder Tiefbau genommen. Willst du dir das wirklich antun?","Bestätigen",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                Person p = new Person
                {
                    Vorname = textBoxFN.Text,
                    Nachname = textBoxLN.Text,
                    Erstwunsch = EW,
                    Zweitwunsch = ZW,
                    Telefonnummer = "" + textBoxAC.Text + numericUpDownNumber.Value
                };
                using (StreamWriter sw = new StreamWriter("Personen.login",true))
                {
                    sw.WriteLine(p);
                }
                MessageBox.Show("Danke für ihre Anmeldung!","Erfolg");
                Application.Exit();
            }
        }

        private void radioButtonEW_Click(object sender, EventArgs e)
        {
            EW = ((RadioButton)sender).Text;
        }
        private void radioButtonZW_Click(object sender, EventArgs e)
        {
            ZW = ((RadioButton)sender).Text;
        }

        private void buttonReadLogins_Click(object sender, EventArgs e)
        {
            if (File.Exists("Personen.login"))
            {
                Form2 f2 = new Form2();
                f2.Show();
            }
            else
                MessageBox.Show("Es hat sich noch keiner Angemeldet!","Fehler");
        }
        private void radioButton_Disable(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder((sender as RadioButton).Name);
            if (sb[11] == 'E')
                sb[11] = 'Z';
            else
                sb[11] = 'E';
            string btn = sb.ToString();
            if ((sender as RadioButton).Checked)
            {
                $"Console.WriteLine('Test')";
            }
            else
            {

            }
        }
    }
}
