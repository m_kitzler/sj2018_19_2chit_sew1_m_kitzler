﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class SVNr
    {
        static string geb;
        int lfnr;
        public SVNr(int Laufnummer, DateTime Geburtsdatum)
        {
            lfnr = Laufnummer;
            geb = Geburtsdatum.ToString("ddMMyy");
        }
        public string Ausgabe()
        {
            if (lfnr < 100 || lfnr > 999)
            {
                throw new Exception("Die Laufnummer muss zwischen 100 und 999 liegen");
            }
            while (true)
            {
                int x = (((lfnr/100) * 3)
                    + (((lfnr/10)%10) * 7)
                    + ((lfnr % 10) * 9)
                    + ((geb[0] - 48) * 5)
                    + ((geb[1] - 48) * 8)
                    + ((geb[2] - 48) * 4)
                    + ((geb[3] - 48) * 2)
                    + ((geb[4] - 48) * 1)
                    + ((geb[5] - 48) * 6)) % 11;
                if (x < 10)
                {
                    return "" + lfnr + x.ToString() + geb[0] + geb[1] + geb[2] + geb[3] + geb[4] + geb[5];
                }
                else
                {
                    lfnr++;
                }
            }
        }
    }
}
