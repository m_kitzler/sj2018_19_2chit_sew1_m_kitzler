﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Indexer_P09102018
{
    class MyArray<T>
    {
        T[] arr;
        public MyArray(T[] arr)
        {
            this.arr = arr;
        }
        public T this[int index]
        {
            get { return arr[index]; }
            set { arr[index] = value; }
        }
    }
}
