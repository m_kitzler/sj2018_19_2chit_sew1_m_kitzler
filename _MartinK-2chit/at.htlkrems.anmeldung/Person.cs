﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace at.htlkrems.anmeldung
{
    public class Person
    {
        public string Vorname { get; set; }
        public string Nachname { get; set; }
        public string Erstwunsch { get; set; }
        public string Zweitwunsch { get; set; }
        public string Telefonnummer { get; set; }
        public Person() { }
        public Person(string generatedValues)
        {
            Vorname = generatedValues.Split(';')[0];
            Nachname = generatedValues.Split(';')[1];
            Erstwunsch = generatedValues.Split(';')[2];
            Zweitwunsch = generatedValues.Split(';')[3];
            Telefonnummer = generatedValues.Split(';')[4];
        }
        public override string ToString()
        {
            return Vorname + ";" + Nachname + ";" + Erstwunsch + ";" + Zweitwunsch + ";" + Telefonnummer;
        }
    }
}
