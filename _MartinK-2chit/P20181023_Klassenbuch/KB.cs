﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klassenbuch_P23102018
{
    class KB
    {
        List<Schueler> schuelerList = new List<Schueler>();
        public void AddSchueler (Schueler schueler)
        {
            schuelerList.Add(schueler);
        }
        public void AddSchueler(Schueler[] schueler)
        {
            foreach(Schueler x in schueler)
            {
                schuelerList.Add(x);
            }
        }
        public Schueler this[int x]
        {
            get { return schuelerList[x]; }
            set { schuelerList[x] = value; }
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < schuelerList.Count; i++)
            {
                sb.Append((i + 1) + ". " + schuelerList[i].GetName() + "\n");
            }
            return sb.ToString();
        }
    }
}
