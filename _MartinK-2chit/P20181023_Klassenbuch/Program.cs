﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Klassenbuch_P23102018
{
    class Program
    {
        static void Main(string[] args)
        {
            KB _2c = new KB();
            _2c.AddSchueler(new Schueler("Max Mustermann", new DateTime(1980, 1, 1)));
            _2c.AddSchueler(new Schueler[]{ new Schueler("Karsten", new DateTime(1980, 1, 1)), new Schueler("I hob mein Namen vagessn", new DateTime(1980, 1, 1))});
            Console.WriteLine(_2c);
        }
    }
}
