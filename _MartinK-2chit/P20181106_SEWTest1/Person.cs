﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SEWTest1_P06112018
{
    class Person
    {
        string vn, nn;
        public Person (string vn, string nn)
        {
            this.vn = vn;
            this.nn = nn;
        }
        public string Vn { set { vn = value; } }
        public string Nn { set { nn = value; } }
        public string FullName { get { return vn + " " + nn; } }
    }
}
