﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace P20112018_Bankkonto
{
    class Jugendkonto:Bankkonto
    {
        double maxBetrag;
        double abgehoben;
        public Jugendkonto(int KNr, double abhebbar):base(KNr)
        {
            maxBetrag = abhebbar;
        }
        public void Auszahlen(double wieviel)
        {
            if (!(abgehoben + wieviel > maxBetrag) && betrag - wieviel > 0)
            {
                betrag -= wieviel;
            }
            else
            {
                throw new Exception("Aha illegal");
            }
        }
        public void Verzinsen()
        {
            
        }
    }
}
