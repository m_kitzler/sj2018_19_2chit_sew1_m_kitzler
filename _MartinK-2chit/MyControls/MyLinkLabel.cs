﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyControls
{
    public class MyLinkLabel : Label
    {
        public Color LinkColor { get; set; } = Color.Purple;
        public override Color ForeColor { get; set; } = Color.Blue;
        public string URL { get; set; }
        protected override void OnMouseEnter(EventArgs e)
        {
            Cursor = Cursors.Hand;
        }
        protected override void OnMouseLeave(EventArgs e)
        {
            Cursor = Cursors.Default;
        }
        protected override void OnClick(EventArgs e)
        {
            ForeColor = LinkColor;
            System.Diagnostics.Process.Start(((URL.Length > 4)&&(URL[4] == ':')) ? "" : "http://" + URL);
        }
    }
}
