﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public class Player
    {
        public Keys[] mapping { get; set; } = new Keys[9];
        public Player() { }
        public Player(string Default)
        {
            if (Default == "DefX")
            {
                Keys[] map = { Keys.Q, Keys.W, Keys.E, Keys.A, Keys.S, Keys.D, Keys.Y, Keys.X, Keys.C };
                Array.Copy(map, mapping, 9);
            }
            if (Default == "DefY")
            {
                Keys[] map = { Keys.NumPad7, Keys.NumPad8, Keys.NumPad9, Keys.NumPad4, Keys.NumPad5, Keys.NumPad6, Keys.NumPad1, Keys.NumPad2, Keys.NumPad3 };
                Array.Copy(map, mapping, 9);
            }
        }
    }
}
