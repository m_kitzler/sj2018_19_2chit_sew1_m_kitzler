﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TicTacToe
{
    public partial class Form2 : Form
    {
        Player PlayerX;
        Player PlayerY;
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(Player x, Player y)
        {
            InitializeComponent();
            PlayerX = x;
            PlayerY = y;
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {

        }
    }
}
