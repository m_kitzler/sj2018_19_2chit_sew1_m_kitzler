﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

enum Direction
{
    None, Up, Right, Down, Left
}
namespace Snake2._0
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.SetBufferSize(120, 30);
            Console.CursorVisible = false;
            int Choice = 1;
            string[] highscore = { "0", "-" };
            string name = "Player";
            bool exit = false,control = true;
            while (!exit)
            {
                while (control)
                {
                    switch (Display.mainMenu(Choice, highscore))
                    {
                        case ConsoleKey.W:
                        case ConsoleKey.UpArrow:
                            if (Choice == 1)
                                Choice = 3;
                            else
                                Choice--;
                            break;
                        case ConsoleKey.S:
                        case ConsoleKey.DownArrow:
                            if (Choice == 3)
                                Choice = 1;
                            else
                                Choice++;
                            break;
                        case ConsoleKey.Enter:
                            control = false;
                            break;
                    }
                    Console.Clear();
                }
                control = true;
                switch (Choice)
                {
                    case 1:
                        int score = Game.Start(60, 30);
                        if (score > Convert.ToInt32(highscore[0]))
                        {
                            highscore[0] = score.ToString();
                            highscore[1] = name;
                        }
                        break;
                    case 2:
                        Console.WriteLine("Gib deinen Namen ein:");
                        Console.CursorVisible = true;
                        name = Console.ReadLine();
                        Console.CursorVisible = false;
                        break;
                    case 3:
                        exit = true;
                        break;
                }
                Choice = 1;
                Console.Clear();
            }
        }
    }
}
