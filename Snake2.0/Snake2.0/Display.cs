﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake2._0
{
    class Display
    {
        static public ConsoleKey mainMenu(int choice, string[] highscore)
        {
            Console.WriteLine("Snake\t\t\tHighscore: {0} by {1}", highscore[0], highscore[1]);
            if (choice == 1)
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.White;
            }
            Console.WriteLine("Spiel starten");
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            if (choice == 2)
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.White;
            }
            Console.WriteLine("Name ändern");
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            if (choice == 3)
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.White;
            }
            Console.WriteLine("Beenden");
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            return Console.ReadKey(true).Key;
        }
        static public void Snake(Snake Snek)
        {
            Console.SetCursorPosition(Snek.Body[Snek.Body.Count - 1].X * 2, Snek.Body[Snek.Body.Count - 1].Y);
            Console.ForegroundColor = ConsoleColor.Black;
            Console.Write("██");
            Console.ForegroundColor = ConsoleColor.White;
            for (int i = Snek.Body.Count - 2; i >= 0; i--)
            {
                Console.SetCursorPosition(Snek.Body[i].X * 2, Snek.Body[i].Y);
                Console.Write("██");
            }
        }
        static public void Point(Item Pos)
        {
            Console.SetCursorPosition(Pos.X * 2, Pos.Y);
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("██");
            Console.ForegroundColor = ConsoleColor.Black;
        }
    }
}
