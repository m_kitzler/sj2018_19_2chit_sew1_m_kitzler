﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace Snake2._0
{
    class Game
    {
        static public int Start(int fieldX, int fieldY)
        {
            bool gameover = false;
            int tick = 0, score = 0;
            Snake snk = new Snake(fieldX, fieldY);
            Point p = new Point(fieldX, fieldY, snk.Body);
            Display.Point(p.NewPosition(snk.Body));
            Direction lastDir = Direction.None;
            while (!gameover)
            {
                if (Console.KeyAvailable)
                {
                    switch (Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.W:
                        case ConsoleKey.UpArrow:
                            if (lastDir != Direction.Up)
                                snk.Move(Direction.Up);
                            lastDir = Direction.Up;
                            break;
                        case ConsoleKey.A:
                        case ConsoleKey.LeftArrow:
                            if (lastDir != Direction.Left)
                                snk.Move(Direction.Left);
                            lastDir = Direction.Left;
                            break;
                        case ConsoleKey.S:
                        case ConsoleKey.DownArrow:
                            if (lastDir != Direction.Down)
                                snk.Move(Direction.Down);
                            lastDir = Direction.Down;
                            break;
                        case ConsoleKey.D:
                        case ConsoleKey.RightArrow:
                            if (lastDir != Direction.Right)
                                snk.Move(Direction.Right);
                            lastDir = Direction.Right;
                            break;
                        case ConsoleKey.Escape:
                            gameover = true;
                            break;
                    }
                    if (!gameover)
                        gameover = snk.Check();
                }
                else
                {
                    Thread.Sleep(10);
                    tick++;
                    if (tick == 5)
                    {
                        tick = 0;
                        snk.Move();
                        gameover = snk.Check();
                    }
                }
                Display.Snake(snk);
                if (snk.First.X == p.Position.X && snk.First.Y == p.Position.Y)
                {
                    score++;
                    Display.Point(p.NewPosition(snk.Body));
                    snk.Grow();
                }
            }
            return score;
        }
    }
}
